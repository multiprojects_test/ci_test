QT -= gui

TEMPLATE = lib
DEFINES += UNTITLED_LIBRARY

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG(debug, debug|release) {
    TARGET = $$join(TARGET,,lib,d)
} else {
    TARGET = $$join(TARGET,,lib)
}

SOURCES += \
    lib.cpp

HEADERS += \
    untitled_global.h \
    lib.h

# Default rules for deployment.
DESTDIR = $$PWD/lib
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
